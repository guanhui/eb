﻿@extends('user/app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/news.css') }}">


    <div class="wrapper">
        <div class="subBanner">
            <img alt="banner" src="{{ \App\Plate::where('flag', '=', 'success')->first()->url }}"></div>
        <div class="container">

            <div class="main">
                <div class="leftSide">
                    @include('user/concact')

                </div>
                <div class="rightSide">
                    <div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.story') }} </span></div>
                    <div class="titleBar">
                        {{ \Illuminate\Support\Facades\Lang::get('message.story') }}
            
                    </div>
                    <div class="content">
                        <h2 id="news_title">
                            @if($lang=='en')
                            {{ $success->title }}
                                @else
                            {{ $success->en_title }}
                                @endif
                        </h2>
                        <div class="content_box">
                            <p>
                                <img src="{{ $success->url }}">
                            </p>
                            <p>
                                @if($lang=='en')
                                    {{ $success->remark }}
                                @else
                                    {{ $success->en_remark }}
                                @endif
                            </p>
                        </div>
                    </div>
                </div>
            </div>
@endsection
