<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>竹蜻蜓</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <script type="text/javascript" src="{{ asset('assets/js/jquery.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/loader-style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}">

    <link href="{{ asset('assets/js/stackable/stacktable.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/js/stackable/responsive-table.css') }}" rel="stylesheet">

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
</head>

<body>   <!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>
    <!-- TOP NAVBAR -->
    <nav role="navigation" class="navbar navbar-static-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button data-target="#bs-example-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="entypo-menu"></span>
                </button>
                <button class="navbar-toggle toggle-menu-mobile toggle-left" type="button">
                    <span class="entypo-list-add"></span>
                </button>




                <div id="logo-mobile" class="visible-xs">
                   <h1>Apricot<span>v1.3</span></h1>
                </div>

            </div>


            <!-- Collect the nav links, forms, and other content for toggling -->
            <div id="bs-example-navbar-collapse-1" class="collapse navbar-collapse">

                <ul style="margin-right:0;" class="nav navbar-nav navbar-right">
                    <li>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <?php echo Auth::user()->name; ?><b class="caret"></b>
                        </a>
                        <ul style="margin-top:14px;" role="menu" class="dropdown-setting dropdown-menu">
                            <!--<li>
                                <a href="">
                                    <span class="entypo-user"></span>&#160;&#160;修改个人信息</a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="icon-information"></span>&#160;&#160;修改网站信息</a>
                            </li>-->
                            <li>
                                <a href="{{ url('web') }}">
                                    <span class="icon-information"></span>&#160;&#160;修改网站信息</a>
                            </li>
                            <li>
                                <a href="{{ url('setting') }}">
                                    <span class="entypo-lifebuoy"></span>&#160;&#160;修改密码</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="{{ url('logout') }}">
                                    <span class="icon-cross"></span>&#160;&#160; 退出</a>
                            </li>
                        </ul>
                    </li>

                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- /END OF TOP NAVBAR -->

    <!-- SIDE MENU -->
    <div id="skin-select">
        <div id="logo">
         <h1>竹蜻蜓</h1>
        </div>

        <a id="toggle">
            <span class="entypo-menu"></span>
        </a>

        <div class="skin-part">
            <div id="tree-wrap">
                <div class="side-bar">
                    <ul id="menu-showhide" class="topnav menu-left-nest">
                        <li>
                            <a href="#" style="border-left:0px solid!important;" class="title-menu-left">
                                <span>网站管理</span>
                                <i data-toggle="tooltip" class="entypo-cog pull-right config-wrap"></i>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('product') }}" title="产品管理">
                                <i class="icon-heart"></i>
                                <span>产品管理</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip2" href="{{ url('newtype') }}" title="新闻类型">
                                <i class="icon-map"></i>
                                <span>新闻类型</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip2" href="{{ url('news') }}" title="新闻列表">
                                <i class="icon-window"></i>
                                <span>新闻中心</span>
                            </a>
                        </li>
<!--
                        <li>
                            <a class="tooltip-tip2" href="{{ url('mediatype') }}" title="视频类型">
                                <i class="icon-preview"></i>
                                <span>视频类型</span>
                            </a>
                        </li>-->

                        <li>
                            <a class="tooltip-tip2" href="{{ url('media') }}" title="视频列表">
                                <i class="icon-monitor"></i>
                                <span>视频中心</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip2" href="{{ url('become') }}" title="成功案例">
                                <i class="icon-feed"></i>
                                <span>成功案例</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('message') }}" title="留言板管理">
                                <i class="icon-calendar"></i>
                                <span>留言板管理</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('flash') }}" title="首页轮播图">
                                <i class="icon-tablet-portrait"></i>
                                <span>首页轮播图</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('plate') }}" title="板块大图">
                                <i class="icon-media-stop"></i>
                                <span>板块大图</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('abouts') }}" title="公司简介">
                                <i class="icon-information"></i>
                                <span>公司简介</span>
                            </a>
                        </li>
<!--
                        <li>
                            <a class="tooltip-tip" href="{{ url('businesses') }}" title="业务范围">
                                <i class="icon-view-list-large"></i>
                                <span>业务范围</span>
                            </a>
                        </li>

                        <li>
                            <a class="tooltip-tip" href="{{ url('ceres') }}" title="资质证书">
                                <i class="icon-view-list"></i>
                                <span>资质证书</span>
                            </a>
                        </li>
-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- END OF SIDE MENU -->

    @yield('content')

    <!-- MAIN EFFECT -->
    <script type="text/javascript" src="{{ asset('assets/js/preloader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/app.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/load.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/main.js') }}"></script>
    <!-- /MAIN EFFECT -->



</body>

</html>
