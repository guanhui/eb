<?php namespace App\Http\Controllers\User;

use App\Business;
use App\Cer;
use App\Flash;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Message;
use App\News;
use App\Product;
use App\Success;
use App\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\User\BaseController;

/**
 * 关于公司、公司证书、联系我们
 * Class CompanyController
 * @package App\Http\Controllers\User
 */

class CompanyController extends BaseController {
    /**
     * 公司首页
     */
    public function index()
    {
        $news = News::orderBy('created_at')->take(5)->get();
        $product = Product::orderBy('created_at')->take(5)->get();
        $flash = Flash::orderBy('sort')->get();
        return view('user.index')->with('product', $product)->with('news', $news)->with('lang', $this->lang)
            ->with('flash', $flash);
    }

    /**
     * 公司简介
     */
    public function about()
    {
        return view('user.about')->with('lang', $this->lang);
    }

    /**
     * 语言切换
     */
    public function language($language)
    {
        App::setLocale($language);
        Session::set('language', $language);
        return Redirect::to('/');
    }

    /**
     * 资质证书
     */
    public function cer()
    {
        $cer = Cer::orderBy('created_at', 'desc')->get();
        return view('user.cer')->with('lang', $this->lang)->with('cer', $cer);
    }

    /**
     * 业务范围
     */
    public function business()
    {
        $business = Business::find(1);
        return view('user.business')->with('lang', $this->lang)->with('business', $business);
    }

    /**
     * 成功案例
     */
    public function success()
    {
        $success = Success::orderBy('created_at', 'desc')->paginate(9);
        return view('user.success')->with('success', $success)->with('lang', $this->lang);
    }


    /**
     * 查看成功案例
     */
    public function successShow($id)
    {
        $success = Success::find($id);
        return view('user.successshow')->with('success', $success)->with('lang', $this->lang);
    }

    /**
     * 联系我们
     */
    public function concact()
    {
        $web = Web::find(1);
        return view('user.content')->with('lang', $this->lang)->with('web', $web);
    }

    /**
     * 留言板
     */
    public function comment()
    {
        return view('user.message')->with('lang', $this->lang);
    }

    /**
     * 留言板处理
     */
    public function commentHandle(Request $request)
    {
        Message::create($request->all());
        return redirect()->back();
    }
}
