<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('login', 'UserController@login');
Route::post('login/post', 'UserController@loginHandle');

Route::get('logout', 'UserController@logout');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middle'=>'auth'], function(){
    Route::resource('news', 'NewController');
    Route::resource('newtype', 'NewTypeController');
    Route::resource('media', 'MediaController');
    Route::resource('mediatype', 'MediaTypeController');
    Route::resource('message', 'MessageController');
    Route::resource('product', 'ProductController');
    Route::resource('product.img', 'ProductImgController');
    Route::resource('become', 'SuccessController');
    Route::resource('web', 'WebController');
    Route::resource('flash', 'FlashController');
    Route::resource('businesses', 'BusinessController');
    Route::resource('plate', 'PlateController');
    Route::resource('ceres', 'CerController');
    Route::resource('abouts', 'AboutController');
    Route::resource('become.img', 'SimgController');
    Route::get('setting', function(){   //修改用户信息
        return view('setting');
    });
    Route::post('setting/handle', 'UserController@setting');
});

Route::group(['namespace'=>'User'], function(){
    Route::get('/', 'CompanyController@index');       //首页
    Route::get('about', 'CompanyController@about');  //关于我们
    Route::get('cer', 'CompanyController@cer');  //资质证书
    Route::resource('goods', 'ProductController');   //商品
    Route::resource('video', 'VideoController');   //视频
    Route::get('video/type/{id}', 'VideoController@type');
    Route::get('business', 'CompanyController@business');  //业务范围
    Route::get('success', 'CompanyController@success');  //成功案例
    Route::get('success/{id}', 'CompanyController@successShow');  //成功案例
    Route::get('concact', 'CompanyController@concact');  //联系我们
    Route::get('comment', 'CompanyController@comment');  //留言
    Route::post('comment/add', 'CompanyController@commentHandle');  //
    Route::resource('newspaper', 'NewsController');
    Route::get('newspaper/type/{id}', 'NewsController@type');
    Route::get('language/{flag}', 'CompanyController@language');
});



