<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model {

	public $fillable = ['name', 'phone', 'email', 'title', 'content', 'status', 'sort',
        'en_name', 'en_title', 'en_content'
    ];

}
