<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Media;
use App\MediaType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MediaController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $media = Media::paginate(10);
        return view('media')->with('media', $media);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('media_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $media = new Media();
        $media->name = $request->input('name');
        $media->remark = $request->input('remark');
        $media->url = $request->input('url');
        $media->display = $request->input('display');
        $media->en_name = $request->input('en_name');
        $media->en_remark = $request->input('en_remark');
        $media->en_url = $request->input('en_url');
        $media->type = $request->input('type');

        if($request->hasFile('img') && $request->file('img')->isValid())
        {
            $upload = $request->file('img');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/media', $filename);
            $media->img = "http://".$_SERVER['SERVER_NAME'].'/upload/media/'.$filename;
        }

        $media->save();
        return Redirect::to('media');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $type = MediaType::all();
        $media = Media::find($id);
        return view('media_show')->with('media', $media)->with('type', $type);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $media = Media::find($id);
        return view('media_edit')->with('media', $media);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $media = Media::find($id);
        $media->name = $request->input('name');
        $media->remark = $request->input('remark');
        $media->url = $request->input('url');
        $media->display = $request->input('display');
        $media->en_name = $request->input('en_name');
        $media->en_remark = $request->input('en_remark');
        $media->en_url = $request->input('en_url');
        $media->type = $request->input('type');

        if($request->hasFile('img') && $request->file('img')->isValid())
        {
            $upload = $request->file('img');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/media', $filename);
            $media->img = "http://".$_SERVER['SERVER_NAME'].'/upload/media/'.$filename;
        }

        $media->save();
        return Redirect::to('media');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Media::find($id)->delete();
        return Redirect::to('media');
    }

}
