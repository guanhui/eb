<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class BaseController extends Controller {

    protected $lang = 'en';  //存储语言

    public function __construct(Request $request)
    {
        if($request->input('lang'))
        {
            App::setLocale($request->input('lang'));
            $this->lang = $request->input('lang');
        }
        else
        {
            App::setLocale($this->lang);
        }

    }
}
