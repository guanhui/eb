﻿@extends('user/app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/news.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
    <style>
        .infoList li{
            height:95px;
        }

        .infoList li dl dd{
            width: 200px;
            height: 70px;
            overflow: hidden;
        }
    </style>
    <div class="wrapper">
        <div class="subBanner">
            <img alt="banner" src="{{ \App\Plate::where('flag', '=', 'success')->first()->url }}"></div>
        <div class="container">

            <div class="main">
                <div class="leftSide">
                    @include('user/concact')
                </div>
                <div class="rightSide">
                    <div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.story') }} </span></div>
                    <div class="titleBar">
                        {{ \Illuminate\Support\Facades\Lang::get('message.story') }}
                    </div>
                    <div class="content">
                        <ul class="infoList">
                            <!--取得一个DataTable-->
                            <!--取得分页页码列表-->
                            @foreach($success as $val)
                            <li>
                                <a href="{{ url('success', [$val->id]) }}"><img alt="" src="{{ $val->url }}"></a>
                                <dl>
                                    <dt><span>{{ $val->created_at }}</span>
                                        <a href="{{ url('success', [$val->id]) }}">
                                        @if($lang=='en')
                                        {{ $val->title }}
                                            @else
                                        {{ $val->en_title }}
                                            @endif
                                        </a>
                                    </dt>
                                    <dd>{{ $val->remark }}</dd>
                                </dl>
                            </li>
                            @endforeach
                        </ul>
                        <div class="cutPage">
                            <?php echo $success->render();?>
                        </div>
                        <!--放置页码列表-->
                    </div>
                </div>
            </div>
@endsection
