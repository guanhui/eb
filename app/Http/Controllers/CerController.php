<?php namespace App\Http\Controllers;

use App\Cer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cer = Cer::orderBy('created_at', 'desc')->paginate(10);
        return view('cer')->with('cer', $cer);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('cer_add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        $cer = new Cer();
        $cer->en_title = $request->input('en_title');
        $cer->title = $request->input('title');
        if($request->hasFile('url') && $request->file('url')->isValid())
        {
            $upload = $request->file('url');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/cer', $filename);
            $cer->url = "http://".$_SERVER['SERVER_NAME'].'/upload/cer/'.$filename;
        }
        $cer->save();
        return Redirect::to('ceres');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$cer = Cer::find($id);
        return view('cer_edit')->with('cer', $cer);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
		$cer = Cer::find($id);
        $cer->title = $request->input('title');
        $cer->en_title = $request->input('en_title');
        if($request->hasFile('url') && $request->file('url')->isValid())
        {
            $upload = $request->file('url');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/cer', $filename);
            $cer->url = "http://".$_SERVER['SERVER_NAME'].'/upload/cer/'.$filename;
        }
        $cer->save();
        return Redirect::to('ceres');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Cer::find($id)->delete();
        return Redirect::to('ceres');
	}

}
