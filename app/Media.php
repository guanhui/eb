<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Media extends Model {

	protected $fillable = ['name', 'remark', 'url', 'display', 'status', 'en_name', 'en_remark', 'en_url', 'type'];

}
