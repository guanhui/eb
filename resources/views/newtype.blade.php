@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <span class="entypo-layout"></span>
                            <span>新闻类型
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                            <!--<div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>-->


                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>首页
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>新闻类型
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->


            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">

                        <div class="nest" id="tableStaticClose">
                            <div class="title-alt">
                                <h6>新闻类型</h6>
                                <div class="titleClose">
                                    <a href="{{ url('newtype') }}/create">添加</a>
                                </div>
                            </div>

                            <div class="body-nest" id="tableStatic">

                                <section id="flip-scroll">

                                    <table class="table table-bordered table-striped cf">
                                        <thead class="cf">
                                            <tr>
                                                <th>编号</th>
                                                <th>名称(中文)</th>
                                                <th>名称(英文)</th>
                                                <th class="numeric">排序</th>
                                                <th class="numeric">操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($type as $v)
                                            <tr>
                                                <td>{{ $v->id }}</td>
                                                <td>{{ $v->name }}</td>
                                                <td>{{ $v->en_name }}</td>
                                                <td class="numeric">{{ $v->sort }}</td>
                                                <td class="numeric">
                                                    <!--<a href="{{ url('newtype', array($v->id)) }}">详细</a>-->
                                                    <a href="{{ url('newtype', array($v->id)) }}/edit">修改</a>
                                                    <form action="{{ url('newtype', array($v->id)) }}" method="post">
                                                        <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <input type="submit" style="border: none; background: none;position:absolute; margin:-19px 0 0 30px" value="删除">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </section>
                                <?php  echo $type->render(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- /END OF CONTENT -->
        </div>
    </div>
    <!--  END OF PAPER WRAP -->
@endsection


