@extends('user/app')
@section('content')
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/products.css') }}">

		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ asset('user/images/productsBanner.jpg') }}">
			</div>
			<div class="container">

				<div class="main">
					<div class="leftSide">
                        @include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.bscope') }} </span>
						</div>
						<div class="titleBar">
                            {{ \Illuminate\Support\Facades\Lang::get('message.bscope') }}
						</div>
						<div class="content">
                            <h2 id="news_title">
                                @if($lang=='en')
                                    {{ $business->title }}
                                @else
                                    {{ $business->en_title }}
                                @endif
                            </h2>

                            <div class="content_box">
                                <p>
                                <span style="font-family: Microsoft YaHei; font-size: 14px;">
                                    @if($lang=='en')
                                        <?php echo $business->content; ?>
                                    @else
                                        <?php echo $business->en_content; ?>
                                    @endif
                                </span>
                                </p>

                            </div>
						</div>
					</div>
				</div>
@endsection