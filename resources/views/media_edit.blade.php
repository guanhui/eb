@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-document-edit"></i> 
                            <span>修改
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                           <!-- <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>
                            -->

                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>首页
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>视频中心
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>修改
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>修改</h6>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#basic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="form-horizontal" enctype="multipart/form-data" role="form"  method="POST" action="{{ url('media', array($media->id)) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PUT">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">类型</label>
                                            <select name="type" class="form-control">
                                                <option @if($media->type == 'product') selected @endif value="product">产品视频</option>
                                                <option @if($media->type == 'aerial') selected @endif  value="aerial">航拍视频</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">标题</label>
                                            <input type="text" name="name" placeholder="请输入标题（选填）" value="{{ $media->name }}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">简介</label>
                                            <textarea name="remark" placeholder="请输入简介（选填）" style="min-height:100px;" class="form-control" rows="10">{{ $media->remark }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">封面</label>
                                            <input type="file" name="img">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">视频地址</label>
                                            <textarea name="url" placeholder="请输入优酷视频地址（选填）" class="form-control" style="min-height:150px;">{{ $media->url }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile">排序</label>
                                            <input type="text" name="display" placeholder="值越大排越前面" class="form-control" value="{{ $media->display }}">
                                        </div>

                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->
            <div class="content-wrap">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>添加(英文)</h6>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#basic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input type="text" name="en_name" placeholder="请输入标题（选填）" value="{{ $media->en_name }}" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Remark</label>
                                        <textarea name="en_remark" placeholder="请输入简介（选填）" style="min-height:100px;" class="form-control" rows="10">{{ $media->en_remark }}</textarea>
                                    </div>

                                    <button class="btn btn-info" type="submit">修改</button>
                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->
        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <link rel="stylesheet" type="text/css" href="{{ asset('/simditor/styles/simditor.css') }}" />
    <script type="text/javascript" src="{{ asset('/simditor/scripts/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/module.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/uploader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/simditor.js') }}"></script>
    <script>
        var editor = new Simditor({
            textarea: $('#editor'),
            upload: true
        });
    </script>

    @endsection
