<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        $this->call('UserTableSeeder');
        $this->call('NewsTableSeeder');
        $this->call('MediaTableSeeder');
        $this->call('ProductsTableSeeder');
        $this->call('ProductImgsTableSeeder');
        $this->call('New_TypesTableSeeder');
        $this->call('SuccessesTableSeeder');
        $this->call('SuccessImgsTableSeeder');
        $this->call('WebsTableSeeder');
        $this->call('FlashTableSeeder');
        $this->call('AboutTableSeeder');
        $this->call('PlateTableSeeder');
	}

}

/**
 * 新闻填充
 * Class NewsTableSeeder
 */
class NewsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('news')->delete();

        \App\News::create([
            'title' => 'Sprite：看起来更像是竹蜻蜓的无人机',
            'from' => '环球网报道',
            'author' => '马丽',
            'en_title' => 'Sprite：It looks more like a dragonfly drone',
            'en_from' => 'foo@bar.com',
            'type_id' => 1,
            'en_author' => 'foo@bar.com',
            'content' => '【环球网报道 实习记者 马丽】据日本共同社5月21日报道，日本东京警视厅少年事件课21日因涉嫌以威力妨碍业务而逮捕了家住横滨市的15岁无业男性少年，原因是此人在网上发布暗示操控小型无人机飞行的视频，妨碍了东京浅草神社的三社祭活动。

据警视厅透露，该少年9日操控小型无人机降落在长野市善光寺内，将对其动机等展开详细调查。此人否认被捕嫌疑称“完全没说过要操控小型无人机”。

该少年于14日至15日在视频网站上发布了“明天浅草好像有活动，我要去参加活动，(活动介绍中)并没写禁止摄像之类”等内容，暗示可能会操纵小型无人机飞行。此举涉嫌迫使15日至17日的三社祭主办方张贴告示禁止使用小型无人机，有妨碍业务之嫌。

警视厅根据少年过去有多次离家出走的经历和21日凌晨外出时的行动等认为其可能逃逸，因此将其控制并逮捕。此外警方还从少年家中没收了一台小型无人机。

版权作品，未经环球网Huanqiu.com书面授权，严禁转载，违者将被追究法律责任。责编：王一',
            'en_content' => "[Global Network reported intern reporter Mary] According to Japan's Kyodo News reported May 21, the Tokyo Metropolitan Police Department Juvenile Division event on the 21st on suspicion of prejudice to the power of business and arrested 15-year-old, who lives in Yokohama, unemployed male youth, because This person posted online hint control UAV flight video, impede the Tokyo Asakusa Shrine Sanja Festival activities.

    According to the Metropolitan Police revealed that the juvenile 9th control UAV landed in Nagano City, good light temple, will launch a detailed investigation of their motivation. This denies the suspects were arrested, he said 'absolutely not said to be controlled UAV.'

The teenagers in the 14 to 15 released on video site with 'tomorrow there are activities like Asakusa, I'm going to participate in activities, (activity presentation) did not write ban cameras and the like', etc., hinted that it might be able to manipulate small unmanned aircraft flying. This allegedly forced three clubs from 15 to 17 festival organizers put up notices prohibiting the use of small UAVs, obstructs business too.

    According to the Metropolitan Police have repeatedly runaway juvenile past experiences and Out Action 21 am and so that it may escape, and therefore its control and arrest. In addition the police also confiscated a small UAV from the juvenile home.

    Copyright works without the global network Huanqiu.com written authorization is forbidden and offenders will be held liable. Zebian: Wang"
        ]);
    }

}

/**
 * 新闻类型填充
 * Class New_TypesTableSeeder
 */
class New_TypesTableSeeder extends Seeder{
    public function run(){
        DB::table('news_types')->delete();
        \App\NewsType::create([
            'name' => '国际新闻',
            'id' => 1,
            'en_name' => 'Nation News'
        ]);
    }
}

/**
 * 视频填充
 * Class MediaTableSeeder
 */
class MediaTableSeeder extends Seeder{
    public function run(){
        DB::table('media')->delete();
        \App\Media::create([
            'name' => '形似竹蜻蜓的Sprite无人机',
            'remark' => '视频: 形似竹蜻蜓的Sprite无人机视频: 形似竹蜻蜓的Sprite无人机 视',
            'url' => '<iframe height=498 width=510 src="http://player.youku.com/embed/XOTY0Mzg2ODE2" frameborder=0 allowfullscreen></iframe>',
            'en_name'=> 'the shape of bamboo dragonfly Sprite UAV',
            'en_remark' => 'the shape of bamboo dragonfly Sprite agonfly Sprite UAV',
            'type' => 'product',
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
    }
}


/**
 * 产品填充
 * Class ProductsTableSeeder
 */
class ProductsTableSeeder extends Seeder{
    public function run()
    {
        DB::table('products')->delete();
        \App\Product::create([
            'id' => 1,
            'name' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_name' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'type' => 'all'
        ]);

        \App\Product::create([
            'id' => 2,
            'name' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_name' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'type' => 'all'
        ]);

        \App\Product::create([
            'id' => 3,
            'name' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_name' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'type' => 'chassis'
        ]);

        \App\Product::create([
            'id' => 4,
            'name' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_name' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'type' => 'ptz'
        ]);

        \App\Product::create([
            'id' => 5,
            'name' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_name' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_content' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'type' => 'other'
        ]);

    }
}

/**
 * 产品图片填充
 * Class ProductImgsTableSeeder
 */
class ProductImgsTableSeeder extends Seeder{
    public function run()
    {
        DB::table('product_imgs')->delete();
        \App\ProductImg::create([
            'product_id' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\ProductImg::create([
            'product_id' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\ProductImg::create([
            'product_id' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\ProductImg::create([
            'product_id' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\ProductImg::create([
            'product_id' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\ProductImg::create([
            'product_id' => 2,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\ProductImg::create([
            'product_id' => 3,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\ProductImg::create([
            'product_id' => 4,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\ProductImg::create([
            'product_id' => 5,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
    }
}

/**
 * 成功填充
 * Class ProductsTableSeeder
 */
class SuccessesTableSeeder extends Seeder{
    public function run()
    {
        DB::table('successes')->delete();
        \App\Success::create([
            'id' => 1,
            'title' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_title' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>'
        ]);

        \App\Success::create([
            'id' => 2,
            'title' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_title' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>'
        ]);

        \App\Success::create([
            'id' => 3,
            'title' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_title' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>'
        ]);

        \App\Success::create([
            'id' => 4,
            'title' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_title' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>'
        ]);

        \App\Success::create([
            'id' => 5,
            'title' => '预售DJI大疆精灵Phantom3遥控航拍无人机4K/HD高清相机四轴飞行器',
            'remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>',
            'en_title' => 'DJI sale Dajiang Wizard Phantom3 remote aerial drones 4K / HD high-definition camera quadrocopter',
            'en_remark' => '<p><p><img alt="Image" src="http://img04.taobaocdn.com/imgextra/i4/684736819/TB2gcnncpXXXXXcXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,560"><br></p><p><img alt="Image" src="http://img02.taobaocdn.com/imgextra/i2/684736819/TB2pBPicpXXXXcdXpXXXXXXXXXX_!!684736819.jpg" data-image-size="800,880"><br></p>'
        ]);

    }
}


/**
 * 成功案例图片填充
 * Class ProductImgsTableSeeder
 */
class SuccessImgsTableSeeder extends Seeder{
    public function run()
    {
        DB::table('success_imgs')->delete();
        \App\SuccessImg::create([
            'success_img' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\SuccessImg::create([
            'success_img' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\SuccessImg::create([
            'success_img' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\SuccessImg::create([
            'success_img' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\SuccessImg::create([
            'success_img' => 1,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\SuccessImg::create([
            'success_img' => 2,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\SuccessImg::create([
            'success_img' => 3,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\SuccessImg::create([
            'success_img' => 4,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
        \App\SuccessImg::create([
            'success_img' => 5,
            'img' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg'
        ]);
    }
}


/**
 * 网站信息
 */
class WebsTableSeeder extends Seeder{
    public function run()
    {
        DB::table('webs')->delete();
        \App\Web::create([
            'id' => 1,
            'name' => '竹蜻蜓',
            'en_name' => 'Bamboo dragonfly',
            'address' => '深圳市南山区阳光里A座2605',
            'en_address' => 'Block A 2605 sunshine Nanshan District, Shenzhen',
            'qq' => '123456',
            'phone' => '0762-123456',
            'email' => '123456@qq.com',
            'logo' => 'http://sz.yun.ftn.qq.com:80/ftn_handler/ebe6d6f9b8ae2d1429b21e723e62b2f19e22589282f14634ce358900ba5b581b/?fname=logo.png&cn=0&cv=30111&size=640*640'
        ]);
    }
}

/**
 * 首页轮播图
 */
class FlashTableSeeder extends Seeder{
    public function run()
    {
        DB::table('flashes')->delete();
        \App\Flash::create([
            'title' => '竹蜻蜓无人机',
            'en_title' => 'Bamboo dreagonfly',
            'url' => 'http://img12.360buyimg.com/n12/jfs/t352/33/384670722/225735/5c85329a/540ec10bN1aa65606.jpg%21q70.jpg',
            'href' => 'http://www.hao123.com'
        ]);
        \App\Flash::create([
            'title' => '竹蜻蜓无人机',
            'en_title' => 'Bamboo dreagonfly',
            'url' => 'http://files.colabug.com/forum/201503/13/135852wsvai27i5alwptp4.jpg',
            'href' => 'http://www.hao123.com'
        ]);
        \App\Flash::create([
            'title' => '竹蜻蜓无人机',
            'en_title' => 'Bamboo dreagonfly',
            'url' => 'http://www.aircraftnurse.com/uploadfile/newsimg/sizec/12%2848%29.jpg',
            'href' => 'http://www.hao123.com'
        ]);
        \App\Flash::create([
            'title' => '竹蜻蜓无人机',
            'en_title' => 'Bamboo dreagonfly',
            'url' => 'http://www.cgai.org.cn/uploadfile/2014/1029/20141029103533739.jpg',
            'href' => 'http://www.hao123.com'
        ]);
        \App\Flash::create([
            'title' => '竹蜻蜓无人机',
            'en_title' => 'Bamboo dreagonfly',
            'url' => 'http://www.cgai.org.cn/uploadfile/2014/1029/20141029103533739.jpg',
            'href' => 'http://www.hao123.com'
        ]);
    }
}

/**
 * 公司简介
 */
class AboutTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('abouts')->delete();
        \App\About::create([
            'id' => 1,
            'content' => '竹蜻蜓。。。',
            'en_content' => 'zhuqingting。。。'
        ]);
    }
}

/**
 * 板块大图
 */
class PlateTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('plates')->delete();
        \App\Plate::create([
            'flag' => 'about',
            'name' => '关于我们',
            'url' => 'http://cd.yun.ftn.qq.com:80/ftn_handler/4561e5aff1873409705e0ef72cca6758cbefc7d3aeaee2d2ac4760b4fa02ccd5/?fname=newsBanner.jpg&cn=0&cv=30111&size=640*640'
        ]);

        \App\Plate::create([
            'flag' => 'news',
            'name' => '新闻中心',
            'url' => 'http://cd.yun.ftn.qq.com:80/ftn_handler/4561e5aff1873409705e0ef72cca6758cbefc7d3aeaee2d2ac4760b4fa02ccd5/?fname=newsBanner.jpg&cn=0&cv=30111&size=640*640'
        ]);

        \App\Plate::create([
            'flag' => 'product',
            'name' => '产品中心',
            'url' => 'http://cd.yun.ftn.qq.com:80/ftn_handler/4561e5aff1873409705e0ef72cca6758cbefc7d3aeaee2d2ac4760b4fa02ccd5/?fname=newsBanner.jpg&cn=0&cv=30111&size=640*640'
        ]);

        \App\Plate::create([
            'flag' => 'video',
            'name' => '视频中心',
            'url' => 'http://cd.yun.ftn.qq.com:80/ftn_handler/4561e5aff1873409705e0ef72cca6758cbefc7d3aeaee2d2ac4760b4fa02ccd5/?fname=newsBanner.jpg&cn=0&cv=30111&size=640*640'
        ]);

        \App\Plate::create([
            'flag' => 'success',
            'name' => '成功案例',
            'url' => 'http://cd.yun.ftn.qq.com:80/ftn_handler/4561e5aff1873409705e0ef72cca6758cbefc7d3aeaee2d2ac4760b4fa02ccd5/?fname=newsBanner.jpg&cn=0&cv=30111&size=640*640'
        ]);

        \App\Plate::create([
            'flag' => 'concact',
            'name' => '联系我们',
            'url' => 'http://cd.yun.ftn.qq.com:80/ftn_handler/4561e5aff1873409705e0ef72cca6758cbefc7d3aeaee2d2ac4760b4fa02ccd5/?fname=newsBanner.jpg&cn=0&cv=30111&size=640*640'
        ]);

        \App\Plate::create([
            'flag' => 'message',  //意见反馈
            'name' => '意见反馈',
            'url' => 'http://cd.yun.ftn.qq.com:80/ftn_handler/4561e5aff1873409705e0ef72cca6758cbefc7d3aeaee2d2ac4760b4fa02ccd5/?fname=newsBanner.jpg&cn=0&cv=30111&size=640*640'
        ]);
    }
}