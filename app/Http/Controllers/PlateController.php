<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Plate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class PlateController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$plate = Plate::paginate(10);
        return view('plate')->with('plate', $plate);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$plate = Plate::find($id);
        return view('plate_show')->with('plate', $plate);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$plate = Plate::find($id);
        return view('plate_edit')->with('plate', $plate);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
        if($request->hasFile('url') && $request->file('url')->isValid())
        {
            $upload = $request->file('url');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/plate', $filename);
            $plate = Plate::find($id);
            $plate->url = "http://".$_SERVER['SERVER_NAME'].'/upload/plate/'.$filename;
            $plate->save();
        }
        return Redirect::to('plate');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
