@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-document-edit"></i> 
                            <span>详细
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                           <!-- <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>
                            -->

                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>首页
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>详细
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>详细</h6>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#basic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="form-horizontal" enctype="multipart/form-data" role="form"  method="POST" action="#">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">标题</label>
                                            <input type="text" name="name" placeholder="请输入标题（选填）" value="{{ $media['attributes']['name'] }}" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">类型</label>
                                            <select name="type_id" class="form-control">
                                                @foreach($type as $key=>$val)
                                                    <option @if($media['attributes']['type_id'] == $val->id) selected @endif value="{{ $val->id }}">{{ $val->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">简介</label>
                                            <textarea name="remark" placeholder="请输入简介（选填）" style="min-height:100px;" class="form-control" rows="10">{{ $media['attributes']['remark'] }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">视频地址</label>
                                            <textarea name="url" placeholder="请输入优酷视频地址（选填）" class="form-control" style="min-height:150px;">{{ $media['attributes']['url'] }}</textarea>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile">排序</label>
                                            <input type="text" name="display" placeholder="值越大排越前面" class="form-control" value="{{ $media['attributes']['display'] }}">
                                        </div>

                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->
            <div class="content-wrap">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>添加(英文)</h6>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#basic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input type="text" name="en_name" placeholder="请输入标题（选填）" value="{{ $media['attributes']['en_name'] }}" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Remark</label>
                                        <textarea name="en_remark" placeholder="请输入简介（选填）" style="min-height:100px;" class="form-control" rows="10">{{ $media['attributes']['en_remark'] }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Media URL</label>
                                        <textarea name="en_url" placeholder="请输入优酷视频地址（选填）" class="form-control" style="min-height:150px;">{{ $media['attributes']['en_url'] }}</textarea>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->

        </div>
    </div>
    <!--  END OF PAPER WRAP -->

@endsection
