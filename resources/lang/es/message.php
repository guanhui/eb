<?php

return [
    'home'      =>  'Home',
    'about'     =>  'About Us',
    'profile'   =>  'Company',
    'cer'       =>  'Certifications',
    'product'   =>  'Products',
    'media'     =>  'Highlights',
    'news'      =>  'News',
    'scope'     =>  'Business',
    'success'   =>  'Success',
    'concact'   =>  'Contact',
    'message'   =>  'Feedback',
    'language'  =>  'language',
    'china'     =>  '中文',
    'english'   =>  'english',
    'keyword'   =>  'Please Enter keywords',

    'cnews'     =>  'Company News',
    'cproduct'  =>  'Company Product',
    'cprofile'  =>  'Company Profile',
    'index'     =>  'Your location',
    'pcenter'    =>  'Products Center',
    'mcenter'     =>  'Highlights Center',
    'ncenter'     =>  'News Center',
    'bscope'     =>  'Business Scope',
    'story'  =>  'Success Story',

    'company_name' => 'Company Name',
    'company_phone' => 'Company Phone',
    'company_qq' => 'Company QQ',
    'company_email' => 'Company Email',
    'company_address' => 'Company Address',

    'video' => 'Videos',
    'pmedia' => 'Product',
    'hmedia' => 'Aerial',

    'all' => 'Overall',
    'chassis' => 'Chassis',
    'ptz' => 'PTZ',
    'equipment' => 'Related Equipment',

    'author' => 'author',
    'from' => 'from',
    'time' => 'published'
];
?>