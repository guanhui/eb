<?php namespace App\Http\Controllers;

use App\Flash;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Web;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class FlashController extends Controller {

	/**
     * 幻灯片管理
     *
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$flash = Flash::orderBy('sort', 'desc')->paginate(10);
        return view('flash')->with('flash', $flash);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('flash_add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$flash = new Flash();
        $flash->title = $request->input('title');
        $flash->en_title = $request->input('en_title');
        $flash->href = $request->input('href');
        if($request->hasFile('url') && $request->file('url')->isValid())
        {
            $upload = $request->file('url');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/flash', $filename);
            $flash->url = "http://".$_SERVER['SERVER_NAME'].'/upload/flash/'.$filename;
        }
        $flash->save();
        return Redirect::to('flash');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$flash = Flash::find($id);
        return view('flash_show')->with('flash', $flash);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $flash = Flash::find($id);
        return view('flash_edit')->with('flash', $flash);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
        $flash = Flash::find($id);
        $flash->title = $request->input('title');
        $flash->en_title = $request->input('en_title');
        $flash->href = $request->input('href');
        if($request->hasFile('url') && $request->file('url')->isValid())
        {
            $upload = $request->file('url');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/flash', $filename);
            $flash->url = "http://".$_SERVER['SERVER_NAME'].'/upload/flash/'.$filename;
        }
        $flash->save();
        return Redirect::to('flash');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Flash::find($id)->delete();
        return Redirect::to('flash');
	}

}
