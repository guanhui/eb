﻿@extends('user/app')
@section('content')
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/products.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
		<!--[if IE]>
    <script src="{{ asset('user/js/html5.js') }}"></script>
    <style type="text/css">
    .clear {
      zoom: 1;
      display: block;
    }
    </style>
<![endif]-->
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/normalize.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/video.css') }}">

		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ \App\Plate::where('flag', '=', 'video')->first()->url }}">
			</div>
			<div class="container">

				<div class="main">
					<div class="leftSide">
                        @include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.mcenter') }} </span>
						</div>
						<div class="titleBar">
                            {{ \Illuminate\Support\Facades\Lang::get('message.mcenter') }}
						</div>
						<div class="video">
							<div class="proClass">
								<ul class="proList">
									<div class="overlay"></div>
                                    @foreach($media as $key=>$val)
									<li>
                                        @if($lang=='en')
                                        {{ $val->name }}
                                        @else
                                            {{ $val->en_name }}
                                        @endif
										<section class="centerUp">
											<img class="medias" data="{{ $val->url }}" alt="{{ $val->name }}" src="{{ $val->img }}">
										</section>
										<p>
                                            @if($lang=='en')
                                            {{ $val->remark }}
                                            @else
                                            {{ $val->en_remark }}
                                            @endif
                                        </p>
										<br>
									</li>
                                    @endforeach

								</ul>
								<div class="cutPage">
                                    <?php echo $media->render()?>
								</div>
								<!--放置页码列表-->
							</div>
						</div>
					</div>
				</div>
                <style>
                    .video2{
                        width: 100%;
                        height: 100%;
                        background: #000;
                        filter:alpha(opacity=90);  /* ie 有效*/
                        -moz-opacity:0.9;   /* Firefox  有效*/
                        opacity: 0.9;    /* 通用，其他浏览器  有效*/
                        position: fixed;
                        top: 0;
                        left: 0;
                        display: none;
                        z-index: 1000;
                    }

                    .media2{
                        background: #fff;
                        position: fixed;
                        top: 150px;
                        left: 35%;
                        opacity: 1;
                        border-radius: 4px;
                        padding: 15px;
                        display: none;
                        z-index: 10000;
                    }

                    #close{
                        position: absolute;
                        right: -7px;
                        top:-15px;
                    }

                </style>
                <div class="video2"></div>
                <div class="media2">
                    <img id="close" src="{{ asset('user/images/close.png') }}" width="30" height="30">
                        <span>
                            <iframe height=498 width=510 src="http://player.youku.com/embed/XOTYwNzMyMDE2" frameborder=0 allowfullscreen></iframe>
                        </span>
                </div>
        <script>
            $('.medias').click(function(){
                $('.media2 span').html($(this).attr('data'));
                $('.video2').show();
                $('.media2').show();
            });

            $('#close').click(function(){
                $('.media2 span').html();
                $('.video2').hide();
                $('.media2').hide();
            });
        </script>
@endsection