<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\MediaType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MediaTypeController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $type = MediaType::paginate(10);
        return view('mediatype')->with('type', $type);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('mediatype_add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        if(MediaType::create($request->all()))
        {
            return Redirect::to('mediatype');
        }
        else
        {
            return redirect()->to();
        }
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$type = MediaType::find($id);
        return view('mediatype_show')->with('type', $type['attributes']);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request,$id)
	{
        $type = MediaType::find($id);
        return view('mediatype_edit')->with('type', $type['attributes']);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
        $type = MediaType::find($id);
        $type->name = $request->input('name');
        $type->sort = $request->input('sort');
        $type->pid = $request->input('pid');
        $type->en_name = $request->input('en_name');
        $type->save();
        return Redirect::to('mediatype');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        MediaType::find($id)->delete();
        return Redirect::to('mediatype');
	}

}
