<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImg extends Model {

	public $fillable = ['product_id', 'img'];
}
