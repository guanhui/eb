<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MediaType extends Model {

	public $fillable = ['name', 'sort', 'pid', 'en_name'];

}
