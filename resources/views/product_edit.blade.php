@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-document-edit"></i> 
                            <span>修改
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                           <!-- <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>
                            -->

                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>首页
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>产品列表
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>修改
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>修改</h6>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#basic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="form-horizontal" enctype="multipart/form-data" role="form"  method="POST" action="{{ url('product', array($product->id)) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PUT">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">类型</label>
                                            <select name="type" class="form-control">
                                                <option @if($product->type == 'all') selected @endif value="all">整体</option>
                                                <option @if($product->type == 'chassis') selected @endif value="chassis">机架</option>
                                                <option @if($product->type == 'ptz') selected @endif value="ptz">云台</option>
                                                <option @if($product->type == 'other') selected @endif value="other">相关器材</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">商品名称</label>
                                            <input type="text" name="name" placeholder="请输入商品名称" class="form-control" value="{{ $product->name }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile">排序</label>
                                            <input type="text" name="sort" placeholder="值越大排越前面" class="form-control" value="{{ $product->sort }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">内容：</label>
                                            <textarea name="content" placeholder="请输入内容" id="editor1" class="form-control" rows="10"><?php echo $product->content; ?></textarea>
                                        </div>

                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->

            <div class="content-wrap">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>添加(英文)</h6>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#basic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="basic">
                                <input type="hidden" id="en_content" name="en_content" value="">
                                <div class="form_center">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Product Name</label>
                                        <input type="text" name="en_name" placeholder="请输入商品名称" class="form-control" value="{{ $product->en_name }}">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Content：</label>
                                        <textarea name="en_content" placeholder="请输入内容" id="editor2" class="form-control" rows="10"><?php echo $product->en_content ?></textarea>
                                    </div>
                                    <button class="btn btn-info" type="submit">修改</button>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->

    <link rel="stylesheet" type="text/css" href="{{ asset('/simditor/styles/simditor.css') }}" />
    <script type="text/javascript" src="{{ asset('/simditor/scripts/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/module.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/uploader.js') }}"></script>
    <script type="text/javascript" src="{{ asset('/simditor/scripts/simditor.js') }}"></script>
    <script>
        var editor1 = new Simditor({
            textarea: $('#editor1'),
            upload: true
        });

        var editor2 = new Simditor({
            textarea: $('#editor2'),
            upload: true
        });

        editor2.on('blur', function(){
            $('input[name="en_content"]').val(editor2.getValue());
        })
    </script>
@endsection
