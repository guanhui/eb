@extends('user/app')
@section('content')
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/message.css') }}">

		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ \App\Plate::where('flag', '=', 'message')->first()->url }}">
			</div>
			<div class="container">
				<div class="main">
					<div class="leftSide">
						<ul class="menu">
							<li class="title">{{ \Illuminate\Support\Facades\Lang::get('message.concact') }}</li>
							<li><a href="{{ url('concact') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.concact') }}</a>
							</li>
							<li><a href="{{ url('comment') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.message') }}</a>
							</li>
						</ul>
						@include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.message') }} </span>
						</div>
						<div class="titleBar">{{ \Illuminate\Support\Facades\Lang::get('message.message') }}</div>
						<div class="message">
							<form action="{{ url('comment/add') }}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <br/>
								<input name="name" type="text" placeholder="您的名字" />
								<input name="phone" type="text" placeholder="您的手机号码" />
								<input name="email" type="text" placeholder="您的邮箱" />
								<textarea name="content" rows="5" placeholder="您需要反馈的内容"></textarea>
                                <br/>
								<input type="submit" style="width:50px;" value="发送">
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
    <br/>
@endsection