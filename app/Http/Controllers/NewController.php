<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\NewsType;
use Illuminate\Support\Facades\Auth;
use App\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class NewController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$news = News::where('status', '=', 1)->paginate(10);
        return view('news')->with('news', $news);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $type = NewsType::all();
		return view('news_add')->with('type', $type);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
        News::create($request->all());
        return Redirect::to('news');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $type = NewsType::all();
		$news = News::find($id);
        return view('news_show')->with('news', $news)->with('type', $type);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $type = NewsType::all();
		$news = News::find($id);
        return view('news_edit')->with('news', $news)->with('type', $type);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$news = News::find($id);
        $news->title = $request->input('title');
        $news->type_id = $request->input('type_id');
        $news->from = $request->input('from');
        $news->author = $request->input('author');
        $news->content = $request->input('content');
        $news->sort = $request->input('sort');
        $news->status = $request->input('status');
        $news->en_title = $request->input('en_title');
        $news->en_from = $request->input('en_from');
        $news->en_author = $request->input('en_author');
        if($request->input('en_content'))
        {
            $news->en_content = $request->input('en_content');
        }

        $news->save();
        return Redirect::to('news');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		News::find($id)->delete();
        return Redirect::to('news');
	}

}
