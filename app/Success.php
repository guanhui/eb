<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Success extends Model {

	public $fillable = [
        'title', 'remark', 'url', 'en_title', 'en_remark'
    ];

    public function simg()
    {
        return $this->hasMany('App\SuccessImg', 'success_img', 'id');
    }
}
