<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsType extends Model {

	public $fillable = ['name', 'pid', 'sort', 'en_name'];

}
