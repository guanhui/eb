﻿@extends('user/app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ url('user/css/reset.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('user/css/news.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('css/page.css') }}">

    <div class="wrapper">
        <div class="subBanner">
            <img alt="banner" src="{{ \App\Plate::where('flag', '=', 'news')->first()->url }}"></div>
        <div class="container">

            <div class="main">
                <div class="leftSide">
                    <ul class="menu">
                        <li class="title">{{ \Illuminate\Support\Facades\Lang::get('message.ncenter') }}</li>
                        @foreach($type as $k=>$v)
                        <li>
                            <a href="{{ url('newspaper/type', [$v->id]) }}?lang={{ $lang }}">
                                @if($lang=='en')
                                {{ $v->name }}
                                    @else
                                {{ $v->en_name }}
                                    @endif
                            </a>
                        </li>
                        @endforeach
                    </ul>
                    @include('user/concact')

                </div>
                <div class="rightSide">
                    <div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.ncenter') }} </span></div>
                    <div class="titleBar">
                        {{ \Illuminate\Support\Facades\Lang::get('message.ncenter') }}
            
                    </div>
                    <div class="content">
                        <ul class="infoList">
                            <!--取得一个DataTable-->
                            <!--取得分页页码列表-->
                            @foreach($news as $key=>$val)
                            <li>
                                <dl>
                                    <dt><span>{{ $val->created_at }}</span><a href="{{ url('newspaper', [$val->id]) }}?lang={{ $lang }}">
                                           @if($lang=='en')
                                            {{ $val->title }}
                                               @else
                                            {{ $val->en_title }}
                                               @endif
                                        </a></dt>
                                </dl>
                            </li>
                            @endforeach
                            
                        </ul>
                        <div class="cutPage">
                            <?php echo $news->render(); ?>
                        </div>
                        <!--放置页码列表-->
                    </div>
                </div>
            </div>
@endsection