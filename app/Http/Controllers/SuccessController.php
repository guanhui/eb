<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SuccessController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$success = Success::orderBy('created_at')->paginate(9);
        return view('success')->with('success', $success);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('success_add');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
    public function store(Request $request)
	{
		$success = new Success();
        $success->title = $request->input('title');
        $success->remark = $request->input('remark');
        $success->en_title = $request->input('en_title');
        $success->en_remark = $request->input('en_remark');
        $success->save();
        return Redirect::to('become');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$success = Success::find($id);
        return view('success_show')->with('success', $success);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request,$id)
	{
        $success = Success::find($id);
        return view('success_edit')->with('success', $success);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $id)
	{
        $success = Success::find($id);
        $success->title = $request->input('title');
        $success->remark = $request->input('remark');
        $success->en_title = $request->input('en_title');
        if($request->input('en_remark')){
            $success->en_remark = $request->input('en_remark');
        }
        $success->save();
        return Redirect::to('become');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Success::find($id)->delete();
        return Redirect::to('become');
	}

}
