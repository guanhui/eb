﻿@extends('user/app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/news.css') }}">

    <div class="wrapper">
        <div class="subBanner">
            <img alt="banner" src="{{ \App\Plate::where('flag', '=', 'news')->first()->url }}"></div>
        <div class="container">

            <div class="main">
                <div class="leftSide">
                    <ul class="menu">
                        <li class="title">{{ \Illuminate\Support\Facades\Lang::get('message.ncenter') }}</li>
                        @foreach($type as $k=>$v)
                            <li>
                                <a href="{{ url('newspaper/type', [$v->id]) }}?lang={{ $lang }}">
                                    @if($lang=='en')
                                        {{ $v->name }}
                                    @else
                                        {{ $v->en_name }}
                                    @endif
                                </a>
                            </li>
                        @endforeach

                    </ul>
                    @include('user/concact')
                </div>
                <div class="rightSide">
                    <div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.ncenter') }} </span></div>
                    <div class="titleBar">
                        {{ \Illuminate\Support\Facades\Lang::get('message.ncenter') }}
            
                    </div>
                    <div class="content">
                        <h2 id="news_title">
                            @if($lang=='en')
                            {{ $news->title }}
                            @else
                            {{ $news->en_title }}
                                @endif
                        </h2>
                        <i class="date_i">{{ \Illuminate\Support\Facades\Lang::get('message.author') }}：
                            @if($lang=='en')
                                {{ $news->author }}
                            @else
                                {{ $news->en_author }}
                            @endif
                            {{ \Illuminate\Support\Facades\Lang::get('message.from') }}：
                            @if($lang=='en')
                                {{ $news->from }}
                            @else
                                {{ $news->en_from }}
                            @endif
                            {{ \Illuminate\Support\Facades\Lang::get('message.time') }}：{{ $news->created_at }}</i>
                        <div class="content_box">
                            <p>
                                <span style="font-family: Microsoft YaHei; font-size: 14px;">
                                    @if($lang=='en')
                                    <?php echo $news->content; ?>
                                        @else
                                        <?php echo $news->en_content; ?>
                                        @endif
                                </span>
                            </p>
                           
                        </div>
                    </div>
                </div>
            </div>
@endsection
