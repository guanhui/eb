<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\NewsType;

class NewTypeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $type = NewsType::paginate(10);
        return view('newtype')->with('type', $type);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('newtype_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        if(NewsType::create($request->all()))
        {
            return Redirect::to('newtype');
        }
        else
        {
            return redirect()->to();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $type = NewsType::find($id);
        return view('newtype_show')->with('type', $type['attributes']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request,$id)
    {
        $type = NewsType::find($id);
        return view('newtype_edit')->with('type', $type['attributes']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $type = NewsType::find($id);
        $type->name = $request->input('name');
        $type->en_name = $request->input('en_name');
        $type->sort = $request->input('sort');
        $type->pid = $request->input('pid');
        $type->save();
        return Redirect::to('newtype');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        NewsType::find($id)->delete();
        return Redirect::to('newtype');
    }


}
