﻿@extends('user/app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ \App\Plate::where('flag', '=', 'about')->first()->url }}">
			</div>
			<div class="container">
				<div class="main">
					<div class="leftSide">
						@include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a> &gt; {{ \Illuminate\Support\Facades\Lang::get('message.about') }} &gt; {{ \Illuminate\Support\Facades\Lang::get('message.cprofile') }}</span>
						</div>
						<div class="titleBar">{{ \Illuminate\Support\Facades\Lang::get('message.cprofile') }}</div>
						<div class="content">
							<p>
                                @if($lang=='en')
								    <?php echo \App\About::find(1)->content; ?>
                                @else
                                    <?php echo \App\About::find(1)->en_content; ?>
                                @endif
							</p>
						</div>
					</div>
				</div>
@endsection