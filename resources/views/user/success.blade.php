﻿@extends('user/app')
@section('content')
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/products.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ \App\Plate::where('flag', '=', 'product')->first()->url }}">
			</div>
			<div class="container">

				<div class="main">
					<div class="leftSide">
						@include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.story') }} </span>
						</div>
						<div class="titleBar">
                            {{ \Illuminate\Support\Facades\Lang::get('message.story') }}
						</div>
						<div class="content">
							<div class="proClass">
								<ul class="proList">
                                    @foreach($success as $key=>$val)
									<li>
                                        @foreach($val->simg()->take(1)->get() as $v)
										<a href="{{ url('success', [$val->id]) }}?lang={{ $lang }}">
											<img src="{{ $v->img }}">
										</a>
                                        @endforeach
										<br>
										<a href="{{ url('success', [$val->id]) }}?lang={{ $lang }}">
                                            @if($lang == 'en')
                                                {{ $val->title }}
                                            @else
                                                {{ $val->en_title }}
                                            @endif
                                        </a>
										<br>
									</li>
                                    @endforeach
								</ul>

                                <?php echo $success->render(); ?>
								<!--放置页码列表-->
							</div>
						</div>
					</div>
				</div>
@endsection