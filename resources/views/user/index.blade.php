﻿@extends('user/app')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{ url('user/css/index.css') }}">
    <link href="{{ url('user/css/jsdaima.css') }}" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="{{ url('user/js/jsdaima.js') }}"></script>
    <div class="wrapper">
			<div class="banner" style="width: 960px; height: 360px;">
				<div id="playBox">
					<div class="pre"></div>
					<div class="next"></div>
					<div class="smalltitle">
						<ul>
                            @foreach($flash as $fk=>$fv)
							    <li @if($fk==0) class="thistitle" @endif></li>
                            @endforeach
						</ul>
					</div>
					<ul class="oUlplay">
                        @foreach($flash as $f)
						<li>
							<a href="{{ $f->href }}">
								<img src="{{ $f->url }}" title="
								@if($lang=='en')
								    {{ $f->title }}
								@else
                                    {{ $f->en_title }}
                                @endif
								" width="980" height="360" />
							</a>
						</li>
                        @endforeach
					</ul>
				</div>

			</div>
			<div class="container">
				<div class="scrollWrapp">
					<div class="title">
						<h3>{{ \Illuminate\Support\Facades\Lang::get('message.cnews') }}</h3>
					</div>
					<div id="scrollDiv" class="rightScroll">
						<ul class="newsList">
                            @foreach($news as $val)
                                <li><a href="{{ url('newspaper', [$val->id]) }}">·
                                        @if($lang=='en')
                                            {{ $val->title }}
                                        @else
                                            {{ $val->en_title }}
                                        @endif
                                    </a><span></span></li>
                            @endforeach
						</ul>

					</div>
					<script type="text/javascript">
						 //滚动插件
						(function($) {
							$.fn.extend({
								Scroll: function(opt, callback) {
									//参数初始化
									if (!opt) var opt = {};
									var _this = this.eq(0).find("ul:first");
									var lineH = _this.find("li:first").height(), //获取行高
										line = opt.line ? parseInt(opt.line, 10) : parseInt(this.height() / lineH, 10), //每次滚动的行数，默认为一屏，即父容器高度
										speed = opt.speed ? parseInt(opt.speed, 10) : 1300, //卷动速度，数值越大，速度越慢（毫秒）
										timer = opt.timer ? parseInt(opt.timer, 10) : 2400; //滚动的时间间隔（毫秒）
									if (line == 0) line = 1;
									var upHeight = 0 - line * lineH;
									//滚动函数
									scrollUp = function() {
											_this.animate({
												marginTop: upHeight
											}, speed, function() {
												for (i = 1; i <= line; i++) {
													_this.find("li:first").appendTo(_this);
												}
												_this.css({
													marginTop: 0
												});
											});
										}
										//鼠标事件绑定
									_this.hover(function() {
										clearInterval(timerID);
									}, function() {
										timerID = setInterval("scrollUp()", timer);
									}).mouseout();
								}
							})
						})(jQuery);
						$(document).ready(function() {
							$("#scrollDiv").Scroll({
								line: 1,
								speed: 1300,
								timer: 2400
							});
						});
					</script>
				</div>
				<!--滚动图片-->
				<div class="main">
					<div class="title">{{ \Illuminate\Support\Facades\Lang::get('message.cproduct') }}</div>
					<div class="proScrollWrapp">
						<div class="leftBtn" onmousedown="ISL_GoUp()" onmouseup="ISL_StopUp()" onmouseout="ISL_StopUp()">
							<img id="leftIcon" alt="左移动" src="{{ url('user/images/leftBtn.jpg') }}">
						</div>
						<div class="proScrollDiv" id="ISL_Cont">
							<div class="ScrCont">
								<ul id="List1" class="proList">
                                    @foreach($product as $key=>$val)
									<li>
                                        @foreach($val->productimg()->take(1)->get() as $v)
                                            <a href="{{ url('goods', [$val->id]) }}">
                                                <img src="{{ $v->img }}" alt="">
                                            </a>
                                        @endforeach
										<dl>
											<dt><a href="{{ url('goods', [$val->id]) }}">
                                                    @if($lang=='en')
                                                        {{ $val->name }}
                                                    @else
                                                        {{ $val->en_name }}
                                                    @endif
                                                </a></dt>
											<dd></dd>
										</dl>
									</li>
                                    @endforeach
								</ul>
							</div>
						</div>
						<div class="rightBtn" onmousedown="ISL_GoDown()" onmouseup="ISL_StopDown()" onmouseout="ISL_StopDown()">
							<img id="rightIcon" alt="右移动" src="{{ url('user/images/rightBtn.jpg') }}">
						</div>
					</div>
					<script language="javascript" src="{{ url('user/js/Scroll.js') }}"></script>
				</div>
@endsection