﻿@extends('user/app')
@section('content')
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/display.css') }}">

		<script type="text/javascript" src="{{ asset('user/js/jquery.jqzoom.js') }}"></script>
		<script type="text/javascript" src="{{ asset('user/js/picture.js') }}"></script>
		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ \App\Plate::where('flag', '=', 'product')->first()->url }}">
			</div>
			<div class="container">
				<div class="main">
					<div class="leftSide">
						@include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.pcenter') }} </span>
						</div>
						<div class="titleBar">
							{{ \Illuminate\Support\Facades\Lang::get('message.pcenter') }}
						</div>
						<div class="content">
							<div class="imgInfo">
								<div class="pictureDIV">
									<div id="preview" class="spec-preview">
										<span class="jqzoom">
                                        @foreach($product->productimg()->take(1)->get() as $key=>$val)
                                        <img jqimg="{{ $val->img }}" alt="">
                                        @endforeach
                                        </span>
									</div>
									<!--缩图开始-->
									<div class="spec-scroll">
										<a class="prev">&lt;</a>
										<a class="next">&gt;</a>
										<div class="items">
											<ul>
                                            @foreach($product->productimg as $k=>$v)
												<li>
													<img bimg="{{ $v->img }}" src="{{ $v->img }}" onmousemove="preview(this);">
												</li>
                                            @endforeach
											</ul>
										</div>
									</div>
									<!--缩图结束-->
								</div>
								<dl class="productItem">
									<dd>
                                        @if($lang=='en')
                                        {{ $product->name }}
                                            @else
                                        {{ $product->en_name }}
                                            @endif
                                    </dd>
								</dl>
							</div>
							<!--Product Information Star-->
							<div class="tabWrap fullInfo">
								<ul class="tab">
									<li class="currentTab">功能特点</li>
									<!--<li class="">技术规格</li>
									<li class="">附件</li>-->
								</ul>
								<div class="productsInfo">
									<div class="tabCon currentCon">
										<p>
                                            @if($lang=='en')
											<?php echo $product->content ?>
                                                @else
                                                <?php echo $product->en_content ?>
                                                @endif
										</p>
									</div>
									<!--<div class="tabCon">
										<p>
											技术规格介绍
										</p>
									</div>
									<div class="tabCon">

										<p>
											附件
										</p>
									</div>-->
								</div>
							</div>
							<script language="javascript" src="{{ asset('user/js/tab.js') }}"></script>
							<!--Product Information End-->
						</div>
					</div>
				</div>
@endsection