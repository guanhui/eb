@extends('user/app')
@section('content')
		<link rel="stylesheet" type="text/css" href="{{ asset('user/about_cer_files/reset.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('user/about_cer_files/news2.css') }}">

		<!--Header End-->
		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ asset('user/about_cer_files/aboutBanner.jpg') }}">
			</div>
			<div class="container">

				<div class="main">
					<div class="leftSide">
						<ul class="menu">
							<li class="title">{{ \Illuminate\Support\Facades\Lang::get('message.about') }}</li>
							<li><a href="{{ url('about') }}">{{ \Illuminate\Support\Facades\Lang::get('message.cprofile') }}</a>
							</li>
							<li><a href="{{ url('cer') }}">{{ \Illuminate\Support\Facades\Lang::get('message.cer') }}</a>
							</li>
						</ul>
						@include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a> &gt;&gt;{{ \Illuminate\Support\Facades\Lang::get('message.about') }}  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.cer') }}</span>
						</div>
						<div class="titleBar">
                            {{ \Illuminate\Support\Facades\Lang::get('message.cer') }}
						</div>
						<div class="content">
							<ul class="infoList">
								<!--取得一个DataTable-->
								<!--取得分页页码列表-->
                                @foreach($cer as $c)
								<li>
									<img alt="@if($lang=='en')
                                            {{ $c->title }}
                                        @else
                                    {{ $c->en_title }}
                                        @endif" src="{{ $c->url }}">
									<dd>
                                        @if($lang=='en')
                                            {{ $c->title }}
                                        @else
                                            {{ $c->en_title }}
                                        @endif
									</dd>
								</li>
                                @endforeach
							</ul>
							<!--<div class="cutPage"><span class="disabled">«上一页</span><span class="current">1</span><a href="#/www.actochina.com/awards/0/2.aspx">2</a><a href="#/www.actochina.com/awards/0/3.aspx">3</a><a href="#/www.actochina.com/awards/0/4.aspx">4</a><span>...</span>
								<a
								href="http://www.actochina.com/awards/0/14.aspx">14</a><a href="#/www.actochina.com/awards/0/2.aspx">下一页»</a>
							</div>-->
							<!--放置页码列表-->
						</div>
					</div>
				</div>
@endsection