<?php

return [
    'home'      =>  '首页',
    'about'     =>  '关于我们',
    'profile'   =>  '公司简介',
    'cer'       =>  '资质证书',
    'product'   =>  '产品中心',
    'media'     =>  '精彩视频',
    'news'      =>  '新闻中心',
    'scope'     =>  '业务范围',
    'success'   =>  '成功案例',
    'concact'   =>  '联系我们',
    'message'   =>  '留言反馈',

    'language'  =>  '语言',
    'china'     =>  '中文',
    'english'   =>  'english',

    'keyword'   =>  '请输入关键字',

    'cnews'     =>  '公司新闻',
    'cproduct'  =>  '公司产品',
    'cprofile'  =>  '公司简介',
    'index'     =>  '您所在的位置',
    'pcenter'   =>  '产品中心',
    'mcenter'   =>  '精彩视频',
    'ncenter'   =>  '新闻中心',
    'bscope'     =>  '业务范围',
    'story'  =>  '成功案例',

    'company_name' => '公司名称',
    'company_phone' => '客服电话',
    'company_qq' => '客服qq',
    'company_email' => '客服邮箱',
    'company_address' => '公司地址',

    'pmedia' => '产品视频',
    'hmedia' => '航拍视频',

    'all' => '整体',
    'chassis' => '机架',
    'ptz' => '云台',
    'equipment' => '相关器材',

    'author' => '作者',
    'from' => '来源',
    'time' => '发布时间'
];

?>