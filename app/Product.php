<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	public $fillable = ['name', 'content', 'sort', 'status', 'en_name','en_content','type'];

    public function productimg()
    {
        return $this->hasMany('App\ProductImg', 'product_id', 'id');
    }

}
