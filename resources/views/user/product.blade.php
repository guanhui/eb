﻿@extends('user/app')
@section('content')
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/products.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/page.css') }}">
		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ \App\Plate::where('flag', '=', 'product')->first()->url }}">
			</div>
			<div class="container">

				<div class="main">
					<div class="leftSide">
                        <ul class="menu">
                            <li class="tit">{{ \Illuminate\Support\Facades\Lang::get('message.pcenter') }}</li>
                            <li><a href="{{ url('goods') }}?lang={{ $lang }}&type=all">{{ \Illuminate\Support\Facades\Lang::get('message.all') }}</a>
                            </li>
                            <li><a href="{{ url('goods') }}?lang={{ $lang }}&type=chassis">{{ \Illuminate\Support\Facades\Lang::get('message.chassis') }}</a>
                            </li>
                            <li><a href="{{ url('goods') }}?lang={{ $lang }}&type=ptz">{{ \Illuminate\Support\Facades\Lang::get('message.ptz') }}</a>
                            </li>
                            <li><a href="{{ url('goods') }}?lang={{ $lang }}&type=other">{{ \Illuminate\Support\Facades\Lang::get('message.equipment') }}</a>
                            </li>
						</ul>
						@include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.pcenter') }} </span>
						</div>
						<div class="titleBar">
                            {{ \Illuminate\Support\Facades\Lang::get('message.pcenter') }}
						</div>
						<div class="content">
                            <!--<div class="proClassLayer">

                                <ul>
                                    <li class="liTitle"><a href="javascript:void(0);">分类</a>
                                    </li>
                                    <li class=""><a href="#">分类</a>
                                    </li>
                                    <li><a href="#">分类</a>
                                    </li>
                                    <li><a href="#">分类</a>
                                    </li>
                                    <li class="liTitle"><a href="javascript:void(0);">分类</a>
                                    </li>
                                <li class=""><a href="#">分类</a>
                                    </li>
                                    <li class=""><a href="#">分类</a>
                                    </li>
                                    <li class=""><a href="#">分类</a>
                                    </li>
                                    <li class=""><a href="#">分类</a>
                                    </li>
                                    <li class=""><a href="#">分类</a>
                                    </li>
                                </ul>
                                <!--<ul class="lastUl">
                                    <li class="liTitle"><a href="javascript:void(0);">续航</a>
                                    </li>
                                    <li class=""><a href="#">1000lm以下</a>
                                    </li>
                                    <li class=""><a href="#">1000lm以下</a>
                                    </li>
                                    <li class=""><a href="#">1000lm以下</a>
                                    </li>
                                    <li class=""><a href="#">1000lm以下</a>
                                    </li>
                                    <li class=""><a href="#">1000lm以下</a>
                                    </li>
                                    <li class=""><a href="#">1000lm以下</a>
                                    </li>
                                    <li class=""><a href="#">1000lm以下</a>
                                    </li>
                                </ul>
                                <script type="text/javascript">
                                    $(".proClassLayer li").hover(function(){
                                                                        $(this).addClass("currentLi");
                                                                    },function(){
                                                                        $(this).removeClass("currentLi");
                                                                    });
                                </script>
                            </div>-->
							<div class="proClass">
								<ul class="proList">
                                    @foreach($product as $key=>$val)
									<li>
                                        @foreach($val->productimg()->take(1)->get() as $v)
										<a href="{{ url('goods', [$val->id]) }}?lang={{ $lang }}">
											<img alt="{{ $val->name }}" src="{{ $v->img }}">
										</a>
                                        @endforeach
										<br>
										<a href="{{ url('goods', [$val->id]) }}?lang={{ $lang }}">
                                            @if($lang == 'en')
                                                {{ $val->name }}
                                            @else
                                                {{ $val->en_name }}
                                            @endif
                                        </a>
										<br>
									</li>
                                    @endforeach
								</ul>

                                <?php echo $product->render(); ?>
								<!--放置页码列表-->
							</div>
						</div>
					</div>
				</div>
@endsection