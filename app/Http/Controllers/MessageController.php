<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class MessageController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $message = Message::paginate(10);
        return view('message')->with('message', $message);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('message_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        Message::create($request->all());
        return Redirect::to('message');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $message = Message::find($id);
        return view('message_show')->with('message', $message);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $message = Message::find($id);
        return view('message_edit')->with('message', $message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request,$id)
    {
        $message = Message::find($id);
        $message->title = $request->input('title');
        $message->name = $request->input('name');
        $message->phone = $request->input('phone');
        $message->content = $request->input('content');
        $message->sort = $request->input('sort');
        $message->email = $request->input('email');
        $message->status = $request->input('status');
        $message->en_name = $request->input('en_name');
        $message->en_title = $request->input('en_title');
        $message->en_content = $request->input('en_content');
        $message->save();
        return Redirect::to('message');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Message::find($id)->delete();
        return Redirect::to('message');
    }

}
