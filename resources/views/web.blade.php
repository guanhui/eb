@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-document-edit"></i> 
                            <span>修改
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                          <!--  <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>
                        -->

                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>


                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>首页
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>修改信息
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>修改</h6>
                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST" action="{{ url('web', [$web->id]) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="hidden" name="_method" value="PUT">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">公司logo</label>
                                            <input type="file" name="logo">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">公司名称（中文）</label>
                                            <input type="text" name="name" placeholder="请输入公司名称（中文）" class="form-control" value="{{ $web->name }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">公司名称（英文）</label>
                                            <input type="text" name="en_name" placeholder="请输入公司名称（英文）" class="form-control" value="{{ $web->en_name }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">客服QQ</label>
                                            <input type="text" name="qq" placeholder="请输入客服QQ" class="form-control" value="{{ $web->qq }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">联系电话</label>
                                            <input type="text" name="phone" placeholder="请输入联系电话" class="form-control" value="{{ $web->phone }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">电子邮箱</label>
                                            <input type="email" name="email" placeholder="请输入电子邮箱" class="form-control" value="{{ $web->email }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">公司地址（中文）</label>
                                            <input type="text" name="address" placeholder="请输入公司地址（中文）" class="form-control" value="{{ $web->address }}">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">公司地址（英文）</label>
                                            <input type="text" name="en_address" placeholder="请输入公司地址（英文）" class="form-control" value="{{ $web->en_address }}">
                                        </div>

                                        <button class="btn btn-info" type="submit">修改</button>
                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->
@endsection
