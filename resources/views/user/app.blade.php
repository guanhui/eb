﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>竹蜻蜓</title>
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/base.css') }}">
        <style>
            .language{
                width: 100px;
                position: absolute;
                right: 10px;
            }

            .language ul{
                width: 100px;
                padding: 5px 0;
                background-color: white;
                border: 1px solid #cccccc;
                border: 1px solid rgba(0,0,0,0.15);
                border-radius: 4px;
                -webkit-box-shadow: 0 6px 12px rgba(0,0,0,0.175);
                box-shadow: 0 6px 12px rgba(0,0,0,0.175);
                position: absolute;
                z-index: 10000;
            }

            .language ul li {
                text-align: center;
                height: 25px;
                line-height: 25px;

            }

            .language ul li:hover{
                background: #e5e5e5;
                cursor: pointer;
            }

            .language span{
                display: inline-block;
                width: 100px;
                height: 20px;
                line-height: 20px;
                text-align: right;
                margin-bottom: 5px;
                cursor: hand;
            }
        </style>
	</head>
	<body>
		<!--Header-->
		<script type="text/javascript" src="{{ asset('user/js/jquery.js') }}"></script>
		<div class="headerLayer">
			<div class="header">
				<div class="top">
					<div class="logo"><img src="{{ \App\Web::find(1)->logo }}" /></div>
                    <div class="seek">
                        <!--<img src="{{ asset('user/images/subBtn.jpg') }}" />
                        <input class="keywords" id="keywords" name="keywords" type="text" value="{{ \Illuminate\Support\Facades\Lang::get('message.keyword') }}" onfocus="javascript:if(this.value==&#39;{{ \Illuminate\Support\Facades\Lang::get('message.keyword') }}&#39;)this.value=&#39;&#39;;" onblur="javascript:if(this.value==&#39;&#39;)this.value=&#39;{{ \Illuminate\Support\Facades\Lang::get('message.keyword') }}&#39;">
                    -->
                    </div>
                    <div class="language">
                        <a href="{{ url('/?lang=en') }}">中文</a>/
                        <a href="{{ url('/?lang=es') }}">English</a>
                    </div>
				</div>
				<ul class="nav" id="nav">
					<li><a title="{{ \Illuminate\Support\Facades\Lang::get('message.home') }}" href="{{ url('/') }}?lang={{ $lang }}"><?php echo \Illuminate\Support\Facades\Lang::get('message.home');?></a> </li>
					<li class=""><a title="{{ \Illuminate\Support\Facades\Lang::get('message.about') }}" href="{{ url('about') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.about') }}</a>
					</li>
                    <li class=""><a title="{{ \Illuminate\Support\Facades\Lang::get('message.news') }}" href="{{ url('newspaper') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.news') }}</a>
                    </li>
					<li class=""><a title="{{ \Illuminate\Support\Facades\Lang::get('message.product') }}" href="{{ url('goods') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.product') }}</a>
                    </li>
					<li class=""><a title="{{ \Illuminate\Support\Facades\Lang::get('message.media') }}" href="#" onclick="return false">{{ \Illuminate\Support\Facades\Lang::get('message.media') }}</a>
                        <ul class="navMenu" style="display: none;">
                            <li class=""><a title="{{ \Illuminate\Support\Facades\Lang::get('message.pmedia') }}" href="{{ url('video') }}?lang={{ $lang }}&type=product">{{ \Illuminate\Support\Facades\Lang::get('message.pmedia') }}</a>
                            </li>
                            <li><a title="{{ \Illuminate\Support\Facades\Lang::get('message.hmedia') }}" href="{{ url('video') }}?lang={{ $lang }}&type=aerial">{{ \Illuminate\Support\Facades\Lang::get('message.hmedia') }}</a>
                            </li>
                        </ul>
                    </li>
					<li class=""><a title="{{ \Illuminate\Support\Facades\Lang::get('message.success') }}" href="{{ url('success') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.success') }}</a>
					</li>
					<li class=""><a title="{{ \Illuminate\Support\Facades\Lang::get('message.product') }}" href="#" onclick="return false">{{ \Illuminate\Support\Facades\Lang::get('message.concact') }}</a>
						<ul class="navMenu" style="display: none;">
							<li class=""><a title="{{ \Illuminate\Support\Facades\Lang::get('message.concact') }}" href="{{ url('concact') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.concact') }}</a>
							</li>
							<li><a title="{{ \Illuminate\Support\Facades\Lang::get('message.message') }}" href="{{ url('comment') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.message') }}</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<script type="text/javascript">
			$(function() {
				$("#nav li").hover(function() {
					$(this).children("ul.navMenu").slideDown();
					$(this).addClass("navHere");
				}, function() {
					$(this).children("ul.navMenu").slideUp();
					$(this).removeClass("navHere");
				});


                var flag = false;
                $(".language span").click(function(){
                    if(!flag){
                        $(".language ul").show();
                        flag = true;
                    }else{
                        $(".language ul").hide();
                        flag = false;
                    }
                });
			});
		</script>

        @yield('content')

        <div class="bottom">
            <span>版权所有  All Rights Reserved.</span>
        </div>
    </div>
</div>
</body>
</html>