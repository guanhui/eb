<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/**
 * 用户登录注册
 * Class UserController
 * @package App\Http\Controllers
 */

class UserController extends Controller {

    /**
     * 登录
     * @return \Illuminate\View\View
     */
	public function login()
    {
        return view('auth.login');
    }

    public function loginHandle(Request $request)
    {
        if(Auth::attempt(['email'=>$request->input('email'), 'password'=>$request->input('password')]))
        {
            return Redirect::to('news');
        }
        else
        {
            return redirect()->back();
        }
    }

    /**
     * 注册
     * @return \Illuminate\View\View
     */
    public function register()
    {
        return view('auth.register');
    }

    public function registerHandle()
    {

    }

    /**
     * 注销
     */
    public function logout()
    {
        Auth::logout();
        return Redirect::to('login');
    }

    /**
     * 修改密码
     */
    public function setting(Request $request)
    {
        //密码输入不正确
        if(!(Hash::check($request->input('opassword'), Auth::user()->password)))
        {
            return redirect()->back();
        }

        if($request->input('password') != $request->input('repassword'))
        {
            return redirect()->back();
        }

        $user = User::find(Auth::user()->id);
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->save();
        return Redirect::to('news');
    }
}
