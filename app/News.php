<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model {

    public $fillable = ['title', 'from', 'author', 'content', 'status', 'sort', 'en_title', 'en_from', 'en_author', 'en_content'];

}
