<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\News;
use App\NewsType;
use Illuminate\Http\Request;
use App\Http\Controllers\User\BaseController;

class NewsController extends BaseController {

    protected $type = '';

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $this->type = NewsType::orderBy('created_at', 'desc')->get();
		$news = News::orderBy('created_at', 'desc')->paginate(9);
        return view('user.news')->with('news', $news)->with('type', $this->type)->with('lang', $this->lang);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $this->type = NewsType::orderBy('created_at', 'desc')->get();
		$news = News::find($id);
        return view('user.newsshow')->with('news', $news)->with('type', $this->type)->with('lang', $this->lang);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    /**
     * 分类查询
     * @param $id
     * @return $this
     */
    public function type($id)
    {
        $this->type = NewsType::orderBy('created_at', 'desc')->get();
        $news = News::where('type_id', '=',$id)->orderBy('created_at', 'desc')->paginate(9);
        return view('user.news')->with('type',$this->type)->with('news', $news)->with('lang', $this->lang);
    }
}
