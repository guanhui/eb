<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Web;
use Illuminate\Http\Request;

class WebController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$webs = Web::find(1);
        return view('web')->with('web', $webs);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request,$id)
	{
		$web = Web::find($id);
        $web->name = $request->input('name');
        $web->en_name = $request->input('en_name');
        $web->address = $request->input('address');
        $web->en_address = $request->input('en_address');
        $web->phone = $request->input('phone');
        $web->qq = $request->input('qq');
        $web->email = $request->input('email');
        if($request->hasFile('logo') && $request->file('logo')->isValid())
        {
            $upload = $request->file('logo');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload', $filename);
            $web->logo = "http://".$_SERVER['SERVER_NAME'].'/upload/'.$filename;
        }
        $web->save();
        return redirect()->back();
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
