<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SuccessImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SimgController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$simg = SuccessImg::where('success_img', '=', $id)->paginate(10);
        return view('simg')->with('simg', $simg)->with('sid', $id);
	}

    public function create($sid)
    {
        return view('simg_add')->with('sid', $sid);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request, $sid)
    {
        $simg = new SuccessImg();
        $simg->success_img = $sid;
        if($request->hasFile('img') && $request->file('img')->isValid())
        {
            $upload = $request->file('img');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/product', $filename);
            $simg->img = "http://".$_SERVER['SERVER_NAME'].'/upload/product/'.$filename;
        }
        $simg->save();
        return Redirect::to('become/'.$sid.'/img');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($sid, $img_id)
    {
        $simg = SuccessImg::find($img_id);
        return view('simg_show')->with('simg', $simg)->with('sid', $sid);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit(Request $request, $sid, $img_id)
    {
        $simg = SuccessImg::find($img_id);
        return view('simg_edit')->with('simg', $simg)->with('sid', $sid);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $sid, $img_id)
    {
        $simg = SuccessImg::find($img_id);
        $simg->success_img = $sid;
        if($request->hasFile('img') && $request->file('img')->isValid())
        {
            $upload = $request->file('img');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/product', $filename);
            $simg->img = "http://".$_SERVER['SERVER_NAME'].'/upload/product/'.$filename;
        }
        $simg->save();
        return Redirect::to('become/'.$sid.'/img');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($sid, $img_id)
    {
        SuccessImg::find($img_id)->delete();
        return Redirect::to('product/'.$sid.'/img');
    }


}
