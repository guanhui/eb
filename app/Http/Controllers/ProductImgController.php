<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;
use App\ProductImg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ProductImgController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index($product_id)
	{
        $img = Product::find($product_id)->productimg()->paginate(10);
        return view('productimg')->with('img', $img)->with('product_id', $product_id);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($product_id)
	{
        return view('productimg_add')->with('product_id', $product_id);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request, $product_id)
	{
        $img = new ProductImg();
        $img->product_id = $product_id;
        if($request->hasFile('img') && $request->file('img')->isValid())
        {
            $upload = $request->file('img');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/product', $filename);
            $img->img = "http://".$_SERVER['SERVER_NAME'].'/upload/product/'.$filename;
        }
        $img->save();
        return Redirect::to('product/'.$product_id.'/img');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($product_id, $img_id)
	{
		$img = ProductImg::find($img_id);
        return view('productimg_show')->with('img', $img['attributes'])->with('product_id', $product_id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(Request $request, $product_id, $img_id)
	{
        $img = ProductImg::find($img_id);
        return view('productimg_show')->with('img', $img['attributes']);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Request $request, $product_id, $img_id)
	{
        $img = ProductImg::find($img_id);
        $img->product_id = $product_id;
        if($request->hasFile('img') && $request->file('img')->isValid())
        {
            $upload = $request->file('img');
            $fileinfo = pathinfo($upload->getClientOriginalName());
            $filename = md5($fileinfo['filename'].time()).'.'.$fileinfo['extension'];
            $upload->move('upload/product', $filename);
            $img->img = 'upload/product/'.$filename;
        }
        $img->save();
        return view('product/'.$product_id.'/img');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($product_id, $img_id)
	{
		ProductImg::find($img_id)->delete();
        return Redirect::to('product/'.$product_id.'/img');
	}

}
