@extends('user/app')
@section('content')
		<link rel="stylesheet" type="text/css" href="{{ asset('user/css/reset.css') }}">

		<div class="wrapper">
			<div class="subBanner">
				<img alt="banner" src="{{ \App\Plate::where('flag', '=', 'concact')->first()->url }}">
			</div>
			<div class="container">
				<div class="main">
					<div class="leftSide">
						<ul class="menu">
							<li class="title">{{ \Illuminate\Support\Facades\Lang::get('message.concact') }}</li>
							<li><a href="{{ url('concact') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.concact') }}</a>
							</li>
							<li><a href="{{ url('comment') }}?lang={{ $lang }}">{{ \Illuminate\Support\Facades\Lang::get('message.message') }}</a>
							</li>
						</ul>
						@include('user/concact')
					</div>
					<div class="rightSide">
						<div class="pagePosition"><span>{{ \Illuminate\Support\Facades\Lang::get('message.index') }}：<a href="{{ url('/') }}">{{ \Illuminate\Support\Facades\Lang::get('message.home') }}</a>  &gt; {{ \Illuminate\Support\Facades\Lang::get('message.concact') }} </span>
						</div>
						<div class="titleBar">{{ \Illuminate\Support\Facades\Lang::get('message.concact') }}</div>
							<p>
								{{ \Illuminate\Support\Facades\Lang::get('company_name') }}：@if($lang=='en'){{ $web->name }}@else{{ $web->en_name }}@endif
							</p>
							<p>
                                {{ \Illuminate\Support\Facades\Lang::get('company_qq') }}：{{ $web->qq }}
							</p>
							<p>
                                {{ \Illuminate\Support\Facades\Lang::get('company_phone') }}：{{ $web->phone }}
							</p>
							<p>
                                {{ \Illuminate\Support\Facades\Lang::get('company_email') }}：{{ $web->email }}
							</p>
							<p>
                                {{ \Illuminate\Support\Facades\Lang::get('company_address') }}：@if($lang=='en'){{ $web->address }}@else{{ $web->en_address }}@endif
							</p>
					</div>
				</div>
			</div>
		</div>
@endsection