<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder{

    public function run()
    {
        DB::table('users')->delete();
        User::create([
            'name' => '梦享科技',
            'email' => 'mengxiang@mx.com',
            'password' => \Illuminate\Support\Facades\Hash::make('mengxiang')
        ]);
    }
}
?>