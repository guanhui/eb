@extends('app')
@section('content')
    <!--  PAPER WRAP -->
    <div class="wrap-fluid">
        <div class="container-fluid paper-wrap bevel tlbr">


            <!-- CONTENT -->
            <!--TITLE -->
            <div class="row">
                <div id="paper-top">
                    <div class="col-sm-3">
                        <h2 class="tittle-content-header">
                            <i class="icon-document-edit"></i> 
                            <span>添加
                            </span>
                        </h2>

                    </div>

                    <div class="col-sm-7">
                        <div class="devider-vertical visible-lg"></div>
                        <div class="tittle-middle-header">

                           <!-- <div class="alert">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <span class="tittle-alert entypo-info-circled"></span>
                                Welcome back,&nbsp;
                                <strong>Dave mattew!</strong>&nbsp;&nbsp;Your last sig in at Yesterday, 16:54 PM
                            </div>
                            -->

                        </div>

                    </div>
                    <div class="col-sm-2">
                        <div class="devider-vertical visible-lg"></div>
                    </div>
                </div>
            </div>
            <!--/ TITLE -->

            <!-- BREADCRUMB -->
            <ul id="breadcrumb">
                <li>
                    <span class="entypo-home"></span>
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>首页
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>首页轮播图
                </li>
                <li><i class="fa fa-lg fa-angle-right"></i>
                </li>
                <li>添加
                </li>
            </ul>

            <!-- END OF BREADCRUMB -->



            <div class="content-wrap">
                <div class="row">


                    <div class="col-sm-12">
                        <div class="nest" id="basicClose">
                            <div class="title-alt">
                                <h6>添加</h6>
                                <div class="titleToggle">
                                    <a class="nav-toggle-alt" href="#basic">
                                        <span class="entypo-up-open"></span>
                                    </a>
                                </div>

                            </div>

                            <div class="body-nest" id="basic">
                                <div class="form_center">
                                    <form class="form-horizontal" enctype="multipart/form-data" role="form"  method="POST" action="{{ url('flash') }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">标题（中文）</label>
                                            <input type="text" name="title" placeholder="请输入标题（中文）" class="form-control" value="">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">标题（英文）</label>
                                            <input type="text" name="en_title" placeholder="请输入标题（英文）" class="form-control" value="">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">链接</label>
                                            <input type="text" name="href" placeholder="请输入链接" class="form-control" value="">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputEmail1">排序</label>
                                            <input type="text" name="sort" class="form-control" value="0">
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputFile">图片</label>
                                            <input type="file" name="url">
                                        </div>

                                        <button class="btn btn-info" type="submit">添加</button>
                                    </form>
                                </div>


                            </div>

                        </div>
                    </div>

                </div>
            </div>

            <!-- /END OF CONTENT -->


        </div>
    </div>
    <!--  END OF PAPER WRAP -->
@endsection
