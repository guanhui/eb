<?php namespace App\Http\Controllers\User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Media;
use App\MediaType;
use Illuminate\Http\Request;
use App\Http\Controllers\User\BaseController;

class VideoController extends BaseController {

	/**
     * 视频 管理
     *
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
        if($request->input('type'))
        {
            $media = Media::where('type', '=', $request->input('type'))->orderBy('created_at', 'desc')->paginate(9);
        }
        else
        {
            $media = Media::where('type', '=', 'product')->orderBy('created_at', 'desc')->paginate(9);
        }

        return view('user.video')->with('media',$media)->with('lang', $this->lang);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

    public function type($id)
    {
        $type = MediaType::orderBy('created_at', 'desc')->get();
        $media = Media::where('type_id', $id)->orderBy('created_at', 'desc')->paginate(9);
        return view('user.video')->with('media',$media)->with('lang', $this->lang)
            ->with('type', $type);
    }

}
